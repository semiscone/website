package models

import (
	"fmt"
	"os"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	log "github.com/sirupsen/logrus"
)

// User table definition
type User struct {
	gorm.Model
	id                      int `gorm:"AUTO_INCREMENT"` // set num to auto incrementable
	Email                   string
	Username                string
	RoleID                  string
	PasswordHash            string
	AccessToken             string
	AppKey                  string
	AppSecret               string
	TokenValid              time.Time
	Confirmed               bool
	Name                    string
	Location                string
	AboutMe                 string
	memberSince             time.Time
	FirstSeen               string
	LastSeen                string
	AvatarHash              string
	Posts                   string
	Releases                string
	LastAuthFailedTime      time.Time
	LastAuthFailedTimes     int
	LastSyncTime            time.Time
	ValidTime               time.Time
	Points                  int
	Phone                   string
	AutoDeliver             bool
	SyncHistory             bool
	AutoDeliverSecuredTrans bool
}

var db *gorm.DB

func initDatabase() (*gorm.DB, error) {
	log.Info("Init database ...")

	host := os.Getenv("POSTGRES_HOST")
	port := 5432
	user := os.Getenv("POSTGRES_USER")
	password := os.Getenv("POSTGRES_PASSWORD")
	database := os.Getenv("POSTGRES_DATABASE")

	conn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, database)
	log.Printf("conn: %s", conn)

	var err error
	db, err = gorm.Open("postgres", conn)
	if err != nil {
		log.Errorf("db status: %v", db)
		log.Errorf("Connect to db failed! ERROR: %v", err.Error())
		return nil, err
	}
	// defer db.Close()

	// Migrate the schema
	db.AutoMigrate(&User{})
	return db, nil
}

// GetUserInfo get user info
func GetUserInfo(username string) (*User, error) {
	db, err := initDatabase()
	if err != nil {
		return nil, err
	}

	var user User
	db.Where("name = ?", username).First(&user)
	return &user, nil
}

// UpdateUserInfo update user info
func UpdateUserInfo(user *User) error {
	db, err := initDatabase()
	if err != nil {
		return err
	}

	err = db.Save(&user).Error
	return err
}

// AddUser add user
func AddUser(user *User) error {
	db, err := initDatabase()
	if err != nil {
		return err
	}

	err = db.Create(user).Error
	return err
}
