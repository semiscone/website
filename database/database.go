package database

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	log "github.com/sirupsen/logrus"
	"gitlab.com/semiscone/website/config"
)

// Init Initialize Database
func Init() (db *gorm.DB, err error) {
	db, err = getSession()
	return
}

func getSession() (db *gorm.DB, err error) {

	connectString := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable", config.DBAddr, config.DBPort, config.DBUser, config.DBName, config.DBPsw)
	// log.Infof("Connect: %v",connectString)
	db, err = gorm.Open("postgres", connectString)

	if err != nil {
		log.Errorf("Connect to databse %v FAILED!!! ERROR: %v", config.DBAddr, err.Error())
		return nil, err
	}

	return
}
