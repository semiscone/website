package controllers

import (
	"net/http"

	"log"

	"github.com/gin-gonic/gin"
)

//RegisterDashboardHandler register dashboard handler
func RegisterDashboardHandler(r *gin.RouterGroup) {
	// private := r.Group("/private")
	// {
	// 	private.GET("/dash", dashboard)
	// 	private.GET("/dashboard", dashboard)
	// }
	// private.Use(AuthRequired())

	r.GET("/d", dashboard)
}

func dashboard(c *gin.Context) {
	log.Print("dashboard")
	c.HTML(http.StatusOK, "dashboard.html", nil)
}
