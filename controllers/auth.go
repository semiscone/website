package controllers

import (
	"bytes"
	"crypto/md5"
	"fmt"
	"io"
	"net/http"
	"regexp"
	"time"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"gitlab.com/semiscone/website/models"
)

// RegisterAuthHandler register auth handler
func RegisterAuthHandler(r *gin.Engine) {
	r.GET("/login", login)
	r.POST("/login", login)

	r.GET("/register", register)
	r.POST("/register", register)
}

func login(c *gin.Context) {
	session := sessions.Default(c)

	if c.Request.Method == "GET" {
		data := gin.H{
			"MsgInfo": session.Flashes("Info"),
			"MsgWarn": session.Flashes("Warn"),
		}
		session.Save()
		c.HTML(http.StatusOK, "login.html", data)
		return
	}

	username := c.PostForm("username")
	password := c.PostForm("password")
	h := md5.New()
	io.WriteString(h, password)
	buffer := bytes.NewBuffer(nil)
	fmt.Fprintf(buffer, "%x", h.Sum(nil))
	inputPass := buffer.String()

	log.Infof("username: %v, password: %v", username, password)
	userInfo, _ := models.GetUserInfo(username)
	if userInfo.PasswordHash != inputPass {
		msg := fmt.Sprintf("The password is not matched!")
		session.AddFlash(msg, "Warn")
		log.Info(msg)
		c.Redirect(http.StatusMovedPermanently, "/login")
	}

	userInfo.LastSeen = time.Now().Format("2006-01-02 15:04:05")
	models.UpdateUserInfo(userInfo)

	session.Set("uid", userInfo.ID)
	session.Set("username", userInfo.Name)
	session.Save()

	c.Redirect(http.StatusMovedPermanently, "/dash")
}

func register(c *gin.Context) {

	session := sessions.Default(c)

	if c.Request.Method == "GET" {
		data := gin.H{
			"MsgInfo": session.Flashes("Info"),
			"MsgWarn": session.Flashes("Warn"),
		}
		session.Save()
		c.HTML(http.StatusOK, "register.html", data)
		return
	}

	username := c.PostForm("username")
	password := c.PostForm("password")
	usererr := checkUsername(username)
	passerr := checkPassword(password)

	fmt.Println(usererr)
	if usererr == false || passerr == false {
		var msg string
		if usererr == false {
			msg = "Password error, Please to again"
		} else {
			msg = "Password error, Please to again"
		}

		session.AddFlash(msg, "Warn")
		session.Save()
		c.Redirect(302, "/regsiter")
		return
	}

	md5Password := md5.New()
	io.WriteString(md5Password, password)
	buffer := bytes.NewBuffer(nil)
	fmt.Fprintf(buffer, "%x", md5Password.Sum(nil))
	newPass := buffer.String()

	userInfo, _ := models.GetUserInfo(username)

	if userInfo.Username != "" {
		msg := "User already exists"
		session.AddFlash(msg, "Warn")
		session.Save()
		c.Redirect(302, "/register")
		return
	}

	now := time.Now().Format("2006-01-02 15:04:05")

	var users models.User
	users.Username = username
	users.PasswordHash = newPass
	users.FirstSeen = now
	users.LastSeen = now
	models.AddUser(&users)

	// Login success and set session
	session.Set("uid", userInfo.ID)
	session.Set("username", userInfo.Name)
	session.Save()
	c.Redirect(302, "/dash")

}

func checkPassword(password string) (b bool) {
	if ok, _ := regexp.MatchString("^[a-zA-Z0-9]{4,16}$", password); !ok {
		return false
	}
	return true
}

func checkUsername(username string) (b bool) {
	if ok, _ := regexp.MatchString("^[a-zA-Z0-9]{4,16}$", username); !ok {
		return false
	}
	return true
}

// Prepare check session id
func Prepare(c *gin.Context) {
	session := sessions.Default(c)
	sessionID := session.Get("uid")
	sessionUsername := session.Get("username")

	if sessionID == nil {
		c.Redirect(302, "/login")
		return
	}

	log.Infof("User [%v] login", sessionUsername)
	c.Next()
}

// AuthRequired to Authenticate User
func AuthRequired(c *gin.Context) {
	session := sessions.Default(c)
	sessionID := session.Get("uid")
	sessionUsername := session.Get("username")
	if sessionID == nil {
		c.Redirect(http.StatusFound, "/login")
		return
	}

	log.Infof("User [%v] login", sessionUsername)
	// Continue down the chain to handler etc
	c.Next()
}

// AuthRequired is a simple middleware to check the session
//  AuthRefuncquired(c *gin.Context) {

// 	token, _ := c.Cookie("X-JWT-ACCESS-TOKEN")
// 	log.Infof("Token: %v", token)

// 	if token == "" {
// 		// Abort the request with the appropriate error code
// 		// c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "unauthorized"})

// 		session := sessions.Default(c)
// 		msg := fmt.Sprintf("Unauthorized user")
// 		session.AddFlash(msg, "Warn")
// 		session.Save()
// 		c.Redirect(http.StatusTemporaryRedirect, "/login")
// 		return
// 	}

// 	c.Next()
// }
