package main

import (
	"net/http"

	log "github.com/sirupsen/logrus"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

// AuthRequired to Authenticate User
func AuthRequired(c *gin.Context) {
	session := sessions.Default(c)
	sessionID := session.Get("uid")
	sessionUsername := session.Get("username")
	if sessionID == nil {
		c.Redirect(http.StatusFound, "/login")
		return
	}

	log.Infof("User [%v] login", sessionUsername)
	// Continue down the chain to handler etc
	c.Next()
}
