package config

import (
	"log"
	"os"

	"github.com/spf13/viper"
)

// name of config file (without extension)
const configFile = "config"

var (
	// ServerPort Listen Port
	ServerPort string
	// DBAddr Database Address
	DBAddr string
	// DBPort Database Port
	DBPort string
	// DBUser Database user
	DBUser string
	// DBPsw Database Password
	DBPsw string
	// DBName Database Name
	DBName string
)

func LoadConfig() {

	viper.SetConfigName(configFile)
	viper.AddConfigPath("./config")
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		log.Fatalf("Fatal error config file: %s", err)
	}

	ServerPort = os.Getenv("WEBSITE_HTTP_PORT")
	if ServerPort == "" {
		ServerPort = ":80"
	}

	DBAddr = os.Getenv("POSTGRES_HOST")
	if DBAddr == "" {
		DBAddr = viper.GetString("postgresql.address")
	}

	DBPort = viper.GetString("postgresql.port")
	DBUser = viper.GetString("postgresql.user")
	DBPsw = viper.GetString("postgresql.password")
	DBName = viper.GetString("postgresql.database")
}
